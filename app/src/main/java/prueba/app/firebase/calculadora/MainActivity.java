package prueba.app.firebase.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText txtNum1;
    private EditText txtNum2;
    private TextView txtResult;
    private Button btnSumar, btnRestar, btnMultiplicar, btnDividir;
    private Button btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones(0.0f,0.0f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        setEvents();
    }
    public void initComponents()
    {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (TextView) findViewById(R.id.txtRes);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDividir = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }

    public void setEvents()
    {
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDividir.setOnClickListener(this);
        this.btnMultiplicar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }

    public int checarCompleto()
    {
        int var = 0;
        if(txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches(""))
        {
            return 1;
        }
        return var;
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnSuma:
                if(checarCompleto() == 1)
                {
                    Toast.makeText(getApplicationContext(),"Favor de LLenar los dos números", Toast.LENGTH_LONG).show();
                }
                else
                {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    Toast.makeText(getApplicationContext(),"Resultado: " + op.suma(), Toast.LENGTH_LONG).show();
                }
                txtResult.setText(String.valueOf(op.suma()));
                break;
            case R.id.btnResta:
                if(checarCompleto() == 1)
                {
                    Toast.makeText(getApplicationContext(),"Favor de LLenar los dos números", Toast.LENGTH_LONG).show();
                }
                else
                {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    Toast.makeText(getApplicationContext(),"Resultado: " + op.resta(), Toast.LENGTH_LONG).show();
                }
                txtResult.setText(String.valueOf(op.resta()));
                break;
            case R.id.btnMult:
                if(checarCompleto() == 1)
                {
                    Toast.makeText(getApplicationContext(),"Favor de LLenar los dos números", Toast.LENGTH_LONG).show();
                }
                else
                {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    Toast.makeText(getApplicationContext(),"Resultado: " + op.mult(), Toast.LENGTH_LONG).show();
                }
                txtResult.setText(String.valueOf(op.resta()));
                break;
            case R.id.btnDivi:
                if(checarCompleto() == 1)
                {
                    Toast.makeText(getApplicationContext(),"Favor de LLenar los dos números", Toast.LENGTH_LONG).show();
                }
                else
                {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    Toast.makeText(getApplicationContext(),"Resultado: " + op.div(), Toast.LENGTH_LONG).show();
                }
                txtResult.setText(String.valueOf(op.resta()));
                break;
            case R.id.btnLimpiar:
                limpiar();
                break;
            case R.id.btnCerrar:
                finish();
                break;

        }
    }

    public void limpiar()
    {
        txtNum1.setText("");
        txtNum2.setText("");
        txtResult.setText("Resultado");
    }
}
